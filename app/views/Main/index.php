<?php require_once 'app/views/_global/beforeContent.php'; ?>

<article class="blok">
    <header>
        <h1>Home page</h1>
    </header>

    <div class="row">
        <div class="col-sm-12 col-md-12">
			Home page content
        </div>
    </div>
</article>

<?php require_once 'app/views/_global/afterContent.php'; ?>