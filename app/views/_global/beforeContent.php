<!doctype html>
<html>
    <head>
        <title><?php echo @$DATA['seo_title']; ?></title>
        <meta charset="utf-8">
        <link href="<?php echo Configuration::BASE; ?>assets/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo Configuration::BASE; ?>assets/css/bootstrap-theme.min.css" rel="stylesheet">
        <link href="<?php echo Configuration::BASE; ?>assets/css/main.css" rel="stylesheet">
        <link href="<?php echo Configuration::BASE; ?>assets/css/<?php echo $FoundRoute['Controller']; ?>.css" rel="stylesheet">
        <link href="<?php echo Configuration::BASE; ?>assets/css/mobile.css" rel="stylesheet">
        <link href="<?php echo Configuration::BASE; ?>assets/css/<?php echo $FoundRoute['Controller']; ?>-mobile.css" rel="stylesheet">
    </head>
    <body>
        <section class="container">
            <nav>
                <ul class="nav nav-tabs">
                    <li><?php Misc::url('', 'Home'); ?></li>
                    <?php if (Session::exists('user_id')): ?>
                    <li><?php Misc::url('logout', 'Log out'); ?></li>
                    <?php else: ?>
                    <li><?php Misc::url('login', 'Log in'); ?></li>
                    <?php endif; ?>
                </ul>
            </nav>
            <main id="sadrzaj">
