<?php
/**
 * Ovo je osnovni kontroler aplikacije koji se koristi za izvrsavanje
 * zahteva upucenih prema podrazumevanim rutama koje poznaje veb sajt.
 */
class MainController extends Controller {
    /**
     * Ovaj metod proverava da li postoje podaci za prijavu poslati HTTP POST
     * metodom, vrsi njihovu validaciju, proverava postojanje korisnika sa tim
     * pristupnim parametrima i u slucaju da sve provere prodju bez greske
     * metod kreira sesiju za korisnika i preusemrava korisnika na default rutu.
     * @return void Metod ne vraca nista, vec koristi return naredbu za prekid izvrsavanja u odredjenim situacijama
     */
    function login() {
        if ($_POST) {
            $username = filter_input(INPUT_POST, 'username');
            $password = filter_input(INPUT_POST, 'password');

            if (!preg_match('/^[a-z0-9]{4,}$/', $username) or !preg_match('/^.{6,}$/', $password)) {
                $this->set('message', 'Invalid username or password format.');
                return;
            }

            $passwordHash = hash('sha512', $password . Configuration::SALT);
            $user = UserModel::getActiveUserByUsernameAndPasswordHash($username, $passwordHash);

            if (!$user) {
                $this->set('message', 'Username or password are incorrect or the user is not active.');
                return;
            }

            Session::set('user_id', $user->user_id);
            Session::set('username', $username);
            Session::set('user_ip', filter_input(INPUT_SERVER, 'REMOTE_ADDR', FILTER_FLAG_IPV4));
            Session::set('user_agent', filter_input(INPUT_SERVER, 'HTTp_USER_AGENT'));

            Misc::redirect('');
        }
    }

    /**
     * Ovaj metod gasi sesiju cime efektivno unistava sve u sesiji,
     * a zatim preusmerava korisnika na stranicu za prijavu na login ruti.
     */
    function logout() {
        Session::end();
        Misc::redirect('login');
    }
}
