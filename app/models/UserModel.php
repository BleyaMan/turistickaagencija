<?php
/**
 * Model koji odgovara tabeli user
 */
class UserModel extends Model {
    /**
     * Metod koji vraca objekat sa podacima korisnika cije korisnicko ime i hes lozinke su dati kao arguemnti metoda
     * @param string $username
     * @param string $passwordHash
     * @return stdClass|NULL
     */
    public static function getActiveUserByUsernameAndPasswordHash($username, $passwordHash) {
        $SQL = 'SELECT * FROM `user` WHERE `username` = ? AND `password` = ? AND `active` = 1;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute([$username, $passwordHash]);
        return $prep->fetch(PDO::FETCH_OBJ);
    }
}
