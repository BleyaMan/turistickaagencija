<?php
class UserLoginModel extends Model {
    public static function getByUserId($id) {
        $id = intval($id);
        $SQL = 'SELECT * FROM user_login WHERE user_id = ?;';
        $prep = DataBase::getInstance()->prepare($SQL);
        $prep->execute([$id]);
        return $prep->fetchAll(PDO::FETCH_OBJ);
    }
}
