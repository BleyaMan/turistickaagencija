<?php
/**
 * Ova klasa predstavlja centralno mesto gde se nalaze svi vazni konfiguracioni
 * parametri ove veb aplikacije, koji su definisani kao clanovi podaci konstante.
 */
final class Configuration {
    const DB_HOST = 'localhost';
    const DB_USER = 'root';
    const DB_PASS = '';
    const DB_BASE = 'sajt';

    const BASE = 'http://localhost/edukativni-php-framework-za-pivt/';
    const PATH = '/edukativni-php-framework-za-pivt/'; # Na sajtu u korenu domena to ce biti samo /

    const SALT = '09t6tuerhgdflgh908348930457yerghoudfhg9348y693486ytdfg';
}
