<?php
    /**
     * Ova datoteka vraca niz asocijativnih nizova koji predstavljaju rute koje
     * postoje u ovoj aplikaciji.
     * <pre>
     * Svaka ruta je asocijativni niz koji mora da sadrzi indekse:
     *  - Pattern    - Regularni izraz koji treba da odgovara zahtevu da se ruta izvrsi
     *  - Controller - Ime kontrolera koji treba koristiti za odgovor zahtevu.
     *                 Ako je ime kontrolera Main, ime klase je MainController.
     *                 Kao vrednost ovog indeksa asocijatvinog niza ide samo Main,
     *                 a ne MainController kako je puno ime klase.
     *  - Method     - Ime metoda izabranog kontrolera koji treba izvrsiti za
     *                 odgovor na pristigli zahtev koji odgovara ovoj ruti.
     * </pre>
     * 
     * Primer:
     * <pre><code>
     * return [
     *   [
     *     'Pattern'    => '|^login/?$|',
     *     'Controller' => 'Main',
     *     'Method'     => 'login'
     *   ],
     *   [
     *     'Pattern'    => '|^logout/?$|',
     *     'Controller' => 'Main',
     *     'Method'     => 'logout'
     *   ],
     *   [ # Poslednja ruta koja ce se izvrsiti ako ni jedna pre ne odgovara pristiglom zahtevu.
     *     'Pattern'    => '|^.*$|',
     *     'Controller' => 'Main',
     *     'Method'     => 'index'
     *   ]
     * ];
     * <code></pre>
     */
    return [
        [
            'Pattern'    => '|^login/?$|',
            'Controller' => 'Main',
            'Method'     => 'login'
        ],
        [
            'Pattern'    => '|^logout/?$|',
            'Controller' => 'Main',
            'Method'     => 'logout'
        ],
        [ # Poslednja ruta!
            'Pattern'    => '|^.*$|',
            'Controller' => 'Main',
            'Method'     => 'index'
        ]
    ];
